require 'csv'
require 'bigdecimal'
require_relative 'item'

class ItemRepository
  def initialize(items:)
    read_csv(items: items)
    return self
  end

  def all
    @items
  end

  def find_by_id(id:)
    find_by_matcher(attr: :id, matcher: id)
  end

  def find_by_name(name:)
    find_by_matcher(attr: :name, matcher: name)
  end

  def find_all_with_description(description:)
    find_all_by_matcher(attr: :description, matcher: description)
  end

  def find_all_by_price(price:)
    find_all_by_exact_matcher(attr: :unit_price, matcher: price)
  end

  def find_all_by_merchant_id(merchant_id:)
    find_all_by_exact_matcher(attr: :merchant_id, matcher: merchant_id)
  end

  def find_all_by_price_in_range(range:)
    @items.select{|i| range.include?(BigDecimal.new(i.unit_price))}
  end

  private

  def find_by_matcher(attr:, matcher:)
    @items.detect{|i| i.send(attr.to_sym).match /#{Regexp.escape(matcher)}/i }
  end

  def find_all_by_matcher(attr:, matcher:)
    @items.select{|i| i.send(attr.to_sym).match /#{Regexp.escape(matcher)}/i }
  end

  def find_all_by_exact_matcher(attr:, matcher:)
    @items.select{|i| i.send(attr.to_sym) == matcher }
  end

  def read_csv(items:)
    @csv ||= CSV.read(items, headers: true, header_converters: :symbol)
    @items ||= @csv.map{|r| Item.new(r.to_h)}
  end
end

