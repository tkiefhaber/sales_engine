require 'csv'
require_relative 'merchant'

class MerchantRepository
  def initialize(merchants:)
    read_csv(merchants: merchants)
    return self
  end

  def all
    @merchants
  end

  def find_by_id(id:)
    find_by_matcher(attr: :id, matcher: id)
  end

  def find_by_name(name:)
    find_by_matcher(attr: :name, matcher: name)
  end

  def find_all_by_name(name:)
    find_all_by_matcher(attr: :name, matcher: name)
  end

  private

  def find_by_matcher(attr:, matcher:)
    @merchants.detect{|i| i.send(attr.to_sym).match /#{Regexp.escape(matcher)}/i }
  end

  def find_all_by_matcher(attr:, matcher:)
    @merchants.select{|i| i.send(attr.to_sym).match /#{Regexp.escape(matcher)}/i }
  end

  def read_csv(merchants:)
    @csv ||= CSV.read(merchants, headers: true, header_converters: :symbol)
    @merchants ||= @csv.map{|r| Merchant.new(r.to_h)}
  end
end

