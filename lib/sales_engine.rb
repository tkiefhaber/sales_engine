require_relative 'item_repository'
require_relative 'merchant_repository'
class SalesEngine

  def self.from_csv(items:, merchants:)
    new(items: items, merchants: merchants)
  end

  attr_accessor :items, :merchants
  def initialize(items:, merchants:)
    @items     = ItemRepository.new(items: items)
    @merchants = MerchantRepository.new(merchants: merchants)
  end
end
