require "minitest/autorun"
require_relative '../lib/item_repository'
require_relative 'test_helper'

describe ItemRepository, :ask do
  before do
    @items_path = File.join(File.dirname(__FILE__), '../data/items.csv')
    @item_repository = ItemRepository.new(items: @items_path)
  end

  describe "query methods" do
    describe "all" do
      it "returns all items" do
        assert_equal @item_repository.instance_variable_get(:@csv).count, @item_repository.all.count
      end
    end

    describe "find_by_id" do
      it "returns nil when no id matches" do
        assert_nil @item_repository.find_by_id(id: 'beef')
      end

      it "returns an instance of Item when id matches" do
        assert_kind_of Item, @item_repository.find_by_id(id: '263395237')
      end
    end

    describe "find_by_name" do
      it "returns nil when no name matches" do
        assert_nil @item_repository.find_by_name(name: 'beef')
      end

      it "returns an instance of Item when name matches" do
        assert_kind_of Item, @item_repository.find_by_name(name: '510+ RealPush Icon Set')
      end
    end

    describe "find_all_with_description" do
      it "returns an empty array with no matches" do
        assert_equal [], @item_repository.find_all_with_description(description: 'beef jerkey')
      end

      it "returns an array of items with matches" do
        assert_equal 706, @item_repository.find_all_with_description(description: 'the').count
      end
    end

    describe "find_all_by_price" do
      it "returns an empty array with no matches" do
        assert_equal [], @item_repository.find_all_by_price(price: '1.2')
      end

      it "returns an array of items with matches" do
        assert_equal 41, @item_repository.find_all_by_price(price: '1200').count
      end
    end

    describe "find_all_by_price_in_range" do
      it "returns an empty array with no matches" do
        assert_equal [], @item_repository.find_all_by_price_in_range(range: 1..2)
      end

      it "returns an array of items with matches" do
        assert_equal 1193, @item_repository.find_all_by_price_in_range(range: 1..20000).count
      end
    end

    describe "find_all_by_merchant_id" do
      it "returns an empty array with no matches" do
        assert_equal [], @item_repository.find_all_by_merchant_id(merchant_id: '1.2')
      end

      it "returns an array of items with matches" do
        assert_equal 1, @item_repository.find_all_by_merchant_id(merchant_id: '12334141').count
      end
    end
  end

end
