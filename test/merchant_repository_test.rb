require "minitest/autorun"
require_relative '../lib/merchant_repository'
require_relative 'test_helper'

describe MerchantRepository, :ask do
  before do
    @merchants_path = File.join(File.dirname(__FILE__), '../data/merchants.csv')
    @merchant_repository = MerchantRepository.new(merchants: @merchants_path)
  end

  describe "query methods" do
    describe "all" do
      it "returns all merchants" do
        assert_equal @merchant_repository.instance_variable_get(:@csv).count, @merchant_repository.all.count
      end
    end

    describe "find_by_id" do
      it "returns nil when no id matches" do
        assert_nil @merchant_repository.find_by_id(id: 'beef')
      end

      it "returns an instance of Merchant when id matches" do
        assert_kind_of Merchant, @merchant_repository.find_by_id(id: '12334105')
      end
    end

    describe "find_by_name" do
      it "returns nil when no name matches" do
        assert_nil @merchant_repository.find_by_name(name: 'beef')
      end

      it "returns an instance of Merchant when name matches" do
        assert_kind_of Merchant, @merchant_repository.find_by_name(name: 'BowlsByChris')
      end
    end

    describe "find_all_by_name" do
      it "returns an empty array with no matches" do
        assert_equal [], @merchant_repository.find_all_by_name(name: '1.2')
      end

      it "returns an array of merchants with matches" do
        assert_equal 1, @merchant_repository.find_all_by_name(name: 'BowlsByChris').count
      end
    end
  end
end
