require "minitest/autorun"
require "pry"
require_relative '../lib/sales_engine'
require_relative 'test_helper'

describe SalesEngine, :ask do
  before do
    @items_path     = File.join(File.dirname(__FILE__), '../data/items.csv')
    @merchants_path = File.join(File.dirname(__FILE__), '../data/merchants.csv')
  end

  describe "initialization" do
    before do
      @error = assert_raises(ArgumentError) { SalesEngine.new }
    end

    it "requires paths to item data file" do
      assert_match /missing keywords\: items/, @error.message
    end

    it "requires paths to merchant data file" do
      assert_match /missing keywords\:.*merchant/, @error.message
    end
  end

  describe "from_csv" do
    it "is a convenience method equal to initialization" do
      assert_kind_of SalesEngine, SalesEngine.from_csv(items: @items_path, merchants: @merchants_path)
    end
  end

  describe "repositories" do
    before do
      @sales_engine   = SalesEngine.new(items: @items_path, merchants: @merchants_path)
    end

    describe "items" do
      it "returns an ItemRepository" do
        assert_kind_of ItemRepository, @sales_engine.items
      end
    end

    describe "merchants" do
      it "returns an MerchantRepository" do
        assert_kind_of MerchantRepository, @sales_engine.merchants
      end
    end
  end
end

